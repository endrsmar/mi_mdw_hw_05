/*
 * Created for MI-MDW course at CTU FIT
 */
package order_processor;

import common.Callback;
import common.Order;
import java.io.Serializable;

/**
 *
 * @author Martin Endrst
 */
public class OrderProcessorCallback implements Callback {
    
    @Override
    public void call(Serializable object){
        try {
            OrderProcessor.process((Order)object);
        } catch (Exception e) {
            System.out.println("Failed to process order");
        }
    }
    
}
