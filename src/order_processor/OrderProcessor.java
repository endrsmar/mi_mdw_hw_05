/*
 * Created for MI-MDW course at CTU FIT
 */
package order_processor;

import common.Order;
import common.QueueReceiverClient;
import common.QueueSenderClient;
import common.Trip;

/**
 * @author Martin Endrst
 */
public class OrderProcessor {
    
    private static final String ALL_QUEUE_NAME = "jms/mdw-05-all";
    private static final String BOOKING_QUEUE_NAME = "jms/mdw-05-booking";
    private static final String TRIP_QUEUE_NAME = "jms/mdw-05-trip";
    
    public static void main(String[] args) throws Exception{
        OrderProcessorCallback callback = new OrderProcessorCallback();
        QueueReceiverClient queueReceiverClient = new QueueReceiverClient(ALL_QUEUE_NAME, callback);
        System.out.println("Starting receiving orders");
        queueReceiverClient.receive();
    }
    
    public static void process(Order order) throws Exception{
        QueueSenderClient queueSenderClient;
        if (order instanceof Trip){
            queueSenderClient = new QueueSenderClient(TRIP_QUEUE_NAME);
            System.out.println("Received Trip, forwarding to trip queue");
        } else {
            queueSenderClient = new QueueSenderClient(BOOKING_QUEUE_NAME);
            System.out.println("Received booking, forwarding to booking queue");
        }
        queueSenderClient.send(order);
        queueSenderClient.close();
    }
    
    
    
}
