/*
 * Created for MI-MDW course at CTU FIT
 */
package order_client;

import common.Booking;
import common.Order;
import common.QueueSenderClient;
import common.Trip;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author Martin Endrst
 */
public class OrderClient {
    private static final String QUEUE_NAME = "jms/mdw-05-all";
    private static final int STATE_INITIAL = 1;
    private static final int STATE_TRIP_DESTINATION = 2;
    private static final int STATE_TRIP_CAPACITY = 3;
    private static final int STATE_TRIP_PRICE = 4;
    private static final int STATE_BOOKING_TRIP_ID = 5;
    private static final int STATE_BOOKING_NAME = 6;
    private static final int STATE_BOOKING_PERSON_COUNT = 7;
    
    public static void main(String[] args) throws Exception{
        QueueSenderClient queueSenderClient = new QueueSenderClient(QUEUE_NAME);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        int state = STATE_INITIAL;
        Order order = new Trip();
        while(true){
            switch (state){
                case STATE_INITIAL:
                    System.out.println("Type trip to create new Trip, Booking to create new Booking or exit to exit");
                    break;
                case STATE_TRIP_DESTINATION:
                    System.out.println("Type name of the destination");
                    break;
                case STATE_TRIP_CAPACITY:
                    System.out.println("Type the trip's capacity");
                    break;
                case STATE_TRIP_PRICE:
                    System.out.println("Type the price in CZK");
                    break;
                case STATE_BOOKING_TRIP_ID:
                    System.out.println("Type ID of the trip being booked");
                    break;
                case STATE_BOOKING_NAME:
                    System.out.println("Type name of the person");
                    break;
                case STATE_BOOKING_PERSON_COUNT:
                    System.out.println("Type the number of persons");
                    break;
            }
            input = bufferedReader.readLine();
            switch (state){
                default:
                case STATE_INITIAL:
                    if (input.equals("exit")){
                        queueSenderClient.close();
                        return;
                    }
                    if (input.equals("trip")){
                        order = new Trip();
                        state = STATE_TRIP_DESTINATION;
                    } else if (input.equals("booking")){
                        order = new Booking();
                        state = STATE_BOOKING_TRIP_ID;
                    }
                    break;
                case STATE_TRIP_DESTINATION:
                    ((Trip)order).setDestination(input);
                    state = STATE_TRIP_CAPACITY;
                    break;
                case STATE_TRIP_CAPACITY:
                    int capacity;
                    try {
                        capacity = Integer.parseInt(input);
                    } catch (Exception e) {
                        System.out.println("Capacity must be a number");
                        break;
                    }
                    ((Trip)order).setCapacity(capacity);
                    state = STATE_TRIP_PRICE;
                    break;
                case STATE_TRIP_PRICE:
                    double price;
                    try {
                        price = Double.parseDouble(input);
                    } catch (Exception e){
                        System.out.println("Price must be a number");
                        break;
                    }
                    ((Trip)order).setPrice(price);
                    queueSenderClient.send(order);
                    System.out.println("Trip sent");
                    state = STATE_INITIAL;
                    break;
                case STATE_BOOKING_TRIP_ID:
                    int id;
                    try {
                        id = Integer.parseInt(input);
                    } catch (Exception e) {
                        System.out.println("Trip ID must be a number");
                        break;
                    }
                    ((Booking)order).setTripId(id);
                    state = STATE_BOOKING_NAME;
                    break;
                case STATE_BOOKING_NAME:
                    ((Booking)order).setName(input);
                    state = STATE_BOOKING_PERSON_COUNT;
                    break;
                case STATE_BOOKING_PERSON_COUNT:
                    int personCount;
                    try {
                        personCount = Integer.parseInt(input);
                    } catch (Exception e) {
                        System.out.println("Person count must be a number");
                        break;
                    }
                    ((Booking)order).setPersonCount(personCount);
                    queueSenderClient.send(order);
                    System.out.println("Booking sent");
                    state = STATE_INITIAL;
                    break;
            }
        }
    }
    
}
