/*
 * Created for MI-MDW course at CTU FIT
 */
package common;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.QueueReceiver;
import javax.naming.NamingException;

/**
 * @author Martin Endrst
 */
public final class QueueReceiverClient extends QueueClient implements MessageListener {
    
    private final QueueReceiver queueReceiver;
    private final Callback callback;
    
    public QueueReceiverClient(String queueName, Callback callback) throws NamingException, JMSException {
        super(queueName);
        this.queueReceiver = this.queueSession.createReceiver(this.queue);
        this.callback = callback;
    }
    
    public void receive() throws JMSException, InterruptedException{
        this.queueReceiver.setMessageListener(this);
        this.queueConnection.start();
        synchronized (this) {
            this.wait();
        }
    }
    
    @Override
    public void onMessage(Message message){
        if (!(message instanceof ObjectMessage)){
            System.out.println("Unexcpected message type");
        } else {
            try {
                this.callback.call(((ObjectMessage)message).getObject());
            } catch (JMSException e){
                System.out.println(e.getMessage());
            }
        }
    }
    
    @Override
    public void close() throws JMSException{
        this.queueReceiver.close();
        super.close();
    }
    
}
