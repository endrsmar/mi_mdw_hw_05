/*
 * Created for MI-MDW course at CTU FIT
 */
package common;

import java.io.Serializable;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.QueueSender;
import javax.naming.NamingException;

/**
 * @author Martin Endrst
 */
public final class QueueSenderClient extends QueueClient {
    
    private final QueueSender queueSender;
    
    public QueueSenderClient(String queueName) throws NamingException, JMSException{
        super(queueName);
        this.queueSender = this.queueSession.createSender(this.queue);
    }
    
    public void send(Serializable object) throws JMSException{
        ObjectMessage objectMessage = this.queueSession.createObjectMessage(object);
        this.queueSender.send(objectMessage, DeliveryMode.PERSISTENT, 8, 0);
        System.out.println("Sent message of type "+object.getClass().getName()+" to the destination "+this.queueSender.getDestination().toString());
    }    
    
    @Override
    public void close() throws JMSException{
        this.queueSender.close();
        super.close();
    }
    
}
