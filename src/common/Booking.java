/*
 * Created for MI-MDW course at CTU FIT
 */
package common;

import java.util.Date;

/**
 * @author Martin Endrst
 */
public class Booking extends Order {
    
    private int tripId;
    private String name;
    private int personCount;
    
    public int getTripId(){
        return this.tripId;
    }
    public String getName(){
        return this.name;
    }
    public int getPersonCount(){
        return this.personCount;
    }
    
    public void setTripId(int tripId){
        this.tripId = tripId;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setPersonCount(int personCount){
        this.personCount = personCount;
    }
    
    public Booking() { }
    
    public Booking(int tripId, String name, int personCount){
        this.tripId = tripId;
        this.name = name;
        this.personCount = personCount;
    }
    
}
