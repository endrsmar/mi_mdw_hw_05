/*
 * Created for MI-MDW course at CTU FIT
 */
package common;

import java.io.Serializable;

/**
 * @author Martin Endrst
 */
public interface Callback {
    public void call(Serializable object);
}
