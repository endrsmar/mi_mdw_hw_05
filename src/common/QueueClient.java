/*
 * Created for the MI-MDW course at CTU FIT
 */
package common;

import java.util.Hashtable;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Martin Endrst
 */
public abstract class QueueClient {
    
    protected final QueueConnectionFactory queueConnectionFactory;
    protected final QueueConnection queueConnection;
    protected final QueueSession queueSession;
    protected final Context context;
    protected final Queue queue;
    
    public QueueClient(String queueName) throws NamingException, JMSException{
        Hashtable<String, String> environment = new Hashtable<String, String>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        environment.put(Context.PROVIDER_URL, Config.PROVIDER_URL);
        this.context = new InitialContext(environment);
        this.queueConnectionFactory = (QueueConnectionFactory) this.context.lookup(Config.JMS_FACTORY);
        this.queueConnection = this.queueConnectionFactory.createQueueConnection();
        this.queueSession = this.queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        this.queue = (Queue) this.context.lookup(queueName);
    }
    
    public void close() throws JMSException {
        this.queueSession.close();
        this.queueConnection.close();
    }
    
}
