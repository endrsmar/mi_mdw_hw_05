/*
 * Created for MI-MDW course at CTU FIT
 */
package common;

/**
 * @author Martin Endrst
 */
public class Trip extends Order {
    
    private String destination;
    private int capacity;
    private double price;
    
    public String getDestination(){
        return this.destination;
    }
    public int getCapacity(){
        return this.capacity;
    }
    public double getPrice(){
        return this.price;
    }
    
    public void setDestination(String destination){
        this.destination = destination;
    }
    public void setCapacity(int capacity){
        this.capacity = capacity;
    }
    public void setPrice(double price){
        this.price = price;
    }
    
    public Trip() { }
    
    public Trip(String destination, int capacity, double price){
        this.destination = destination;
        this.capacity = capacity;
        this.price = price;
    }
    
}
