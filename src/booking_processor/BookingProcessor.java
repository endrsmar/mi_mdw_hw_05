/*
 * Created for MI-MDW course at CTU FIT
 */
package booking_processor;

import common.Booking;
import common.QueueReceiverClient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Endrst
 */
public class BookingProcessor {
    
    private static final String QUEUE_NAME = "jms/mdw-05-booking";
    private static List<Booking> database = new ArrayList<Booking>();
    
    public static void main(String[] args) throws Exception{
        BookingProcessorCallback callback = new BookingProcessorCallback();
        QueueReceiverClient queueReceiverClient = new QueueReceiverClient(QUEUE_NAME, callback);
        System.out.println("Starting receiving bookings");
        queueReceiverClient.receive();
    }
    
    public static void addBooking(Booking booking){
        database.add(booking);
        System.out.println("Added booking by "+booking.getName()+" for "+Integer.toString(booking.getPersonCount())+" to the trip with ID "+Integer.toString(booking.getTripId()));
    }
    
}
