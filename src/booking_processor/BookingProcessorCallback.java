/*
 * Created for MI-MDW course at CTU FIT
 */
package booking_processor;

import common.Booking;
import common.Callback;
import java.io.Serializable;

/**
 * @author Martin Endrst
 */
public class BookingProcessorCallback implements Callback{
    
    @Override
    public void call(Serializable object){
        BookingProcessor.addBooking((Booking)object);
    }
    
}
