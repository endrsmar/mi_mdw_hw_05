/*
 * Created for MI-MDW course at CTU FIT
 */
package trip_processor;

import common.QueueReceiverClient;
import common.Trip;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Endrst
 */
public class TripProcessor {
    
    private static final String QUEUE_NAME = "jms/mdw-05-trip";
    private static List<Trip> database = new ArrayList<Trip>();
    
    public static void main(String[] args) throws Exception{
        TripProcessorCallback callback = new TripProcessorCallback();
        QueueReceiverClient queueReceiverClient = new QueueReceiverClient(QUEUE_NAME, callback);
        System.out.println("Starting receiving trips");
        queueReceiverClient.receive();
    }
    
    public static void addTrip(Trip trip){
        database.add(trip);
        System.out.println("Added trip to "+trip.getDestination()+" with capacity of "+Integer.toString(trip.getCapacity())+" persons at the price of "+Double.toString(trip.getPrice())+" CZK");
    }
    
}
