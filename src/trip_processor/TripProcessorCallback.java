/*
 * Created for MI-MDW course at CTU FIT
 */
package trip_processor;

import common.Callback;
import common.Trip;
import java.io.Serializable;

/**
 * @author Martin Endrst
 */
public class TripProcessorCallback implements Callback{
    
    @Override
    public void call(Serializable object){
        TripProcessor.addTrip((Trip)object);
    }
    
}
